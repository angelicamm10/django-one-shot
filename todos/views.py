from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TaskForm
#from django.http import HttpResponse

# Create your views here.
def todo_list_list(request):
    todolist= TodoList.objects.all()
    context ={
        "todo_list_list": todolist
    }
    return render(request, "todos/todo_lists.html", context)
    #eturn HttpResponse ('this view is working')

def todo_list_detail(request,id):
    item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item" : item
    }
    return render(request, "todos/detail.html", context)

def create_todolist (request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list =form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else: 
        form = TodoListForm()
    context = {
        "form":form
    }
    return render(request, "todos/create.html", context)


def edit_todolist(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method =="POST":
        form= TodoListForm(request.POST, instance = todo_list)
        print(form.is_valid())
        if form.is_valid():
            todo_list= form.save()
            print("it works!")
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance = todo_list)
    context ={
        "todo_list": todo_list,   
        "form": form, 
    }

    return render (request, "todos/edit.html", context)


def delete_list(request, id):
    todo_list = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect ("todo_list_list")
    
    return render(request, "todos/delete.html")
    
def create_task(request,):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TaskForm()
    context ={
        "form":form
    }
    return render(request, "todos/create_item.html", context)


def edit_todoitem(request, id):
    todo_item = TodoList.objects.get(id=id)
    if request.method =="POST":
        form= TaskForm(request.POST, instance = todo_item)
        print(form.is_valid())
        if form.is_valid():
            todo_item= form.save()
            # print("it works!")
            return redirect("todo_list_detail", id=todo_item.id)
    else:
        form = TaskForm(instance = todo_item)
    context ={
        "todo_item": todo_item,   
        "form": form, 
    }

    return render (request, "todos/edit_item.html", context)
