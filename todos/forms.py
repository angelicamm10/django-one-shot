from django.forms import ModelForm
from django import forms 
from todos.models import TodoList, TodoItem

class TodoListForm(ModelForm):
    class Meta:
        model=TodoList
        fields = [
            "name",

        ]


# class TaskForm(forms.Form):
#         task =forms.CharField(label= "task", max_length=100, required=False)
#         due_date = forms.DateTimeField
#         is_completed= forms.CheckboxInput

        


class TaskForm(ModelForm):
    class Meta:
        model=TodoItem 
        fields = [
            "task",
            "due_date",
            "list",
            "is_completed"
        ]
      
